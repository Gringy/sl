#include <QPainter>
#include "Drawer.h"

#include <iostream>

Drawer *defaultDrawer = NULL;

Drawer::Drawer(QWidget *) : pixmap(512, 512) {
	setMinimumSize(512, 512);
	setMaximumSize(512, 512);
	defaultDrawer = this;
	color = Qt::white;
}

void Drawer::paintEvent(QPaintEvent *) {
	QPainter painter(this);
	painter.drawPixmap(0, 0, pixmap);
}

void Drawer::drawEllipse(int x, int y, int size) {
	QPainter painter(&pixmap);
	painter.setBrush(color);	
	painter.drawEllipse(x, y, size, size);
}

void Drawer::drawPoint(int x, int y) {
	QPainter painter(&pixmap);
	painter.setBrush(color);
	QPen pen(color);
	painter.setPen(pen);
	painter.drawPoint(QPoint(x, y));
}

void Drawer::clear() {
	pixmap.fill(color);
}

void Drawer::setColor(int r, int g, int b) {
	color = QColor(r, g, b);
}