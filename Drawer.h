#pragma once

#include <QWidget>
#include <QPixmap>

class Drawer : public QWidget {
	Q_OBJECT
public:
	Drawer(QWidget *parent = 0);
	void paintEvent(QPaintEvent *);

	// for words
	void drawEllipse(int x, int y, int size);
	void drawPoint(int x, int y);
	void setColor(int r, int g, int b);
	void clear();
private:
	QColor color;
	QPixmap pixmap;
};

extern Drawer *defaultDrawer;