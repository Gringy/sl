#include "MainWindow.h"
#include <QFileDialog>
#include "interpreter.h"

#include <iostream>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
	timer = new QTimer(this);

	startButton = new QPushButton();
	startButton->setText("GO");
	stopButton = new QPushButton();
	stopButton->setText("STOP");
	stopButton->setEnabled(false);
	loadButton = new QPushButton();
	loadButton->setText("LOAD");

	vLay = new QVBoxLayout();
	hLay = new QHBoxLayout();

	hLay->addWidget(startButton);
	hLay->addWidget(stopButton);
	hLay->addWidget(loadButton);

	vLay->addLayout(hLay);
	vLay->setAlignment(Qt::AlignTop);

	drawer = new Drawer();
	vLay->addWidget(drawer);

	QWidget *widget = new QWidget();
	widget->setLayout(vLay);
	setCentralWidget(widget);

	QWidget::connect(startButton, SIGNAL(clicked(bool)), this, SLOT(start_clicked(bool)));
	QWidget::connect(stopButton, SIGNAL(clicked(bool)), this, SLOT(stop_clicked(bool)));
	QWidget::connect(loadButton, SIGNAL(clicked(bool)), this, SLOT(load_clicked(bool)));
	QWidget::connect(timer, SIGNAL(timeout()), this, SLOT(timer_tick()));
}

void MainWindow::start_clicked(bool) {
	if (inter->IsRunning()) {
		timer->start(50);
		startButton->setEnabled(false);
		stopButton->setEnabled(true);
	}
}

void MainWindow::stop_clicked(bool) {
	stopButton->setEnabled(false);
	startButton->setEnabled(true);
	timer->stop();
}

void MainWindow::load_clicked(bool) {
	timer->stop();
	QString str = QFileDialog::getOpenFileName(this, "Open Source", "", "Source (*.sl)");
	delete inter;
	inter = new Interpreter(str.toStdString().c_str());
	drawer->setColor(255, 255, 255);
	drawer->clear();
	startButton->setEnabled(true);
	stopButton->setEnabled(false);
}

void MainWindow::timer_tick() {
	if (!inter->IsRunning()) {
		timer->stop();
		stopButton->setEnabled(false);
		startButton->setEnabled(false);
		return;
	}
	for (int i = 0; i < 1000 && inter->IsRunning(); i++)
		inter->Tick();
}