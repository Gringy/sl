#pragma once

#include <QMainWindow>
#include <QTimer>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include "Drawer.h"

class MainWindow : public QMainWindow {
	Q_OBJECT
public:
	MainWindow(QWidget *parent = 0);
public slots:
	void timer_tick();
	void start_clicked(bool);
	void stop_clicked(bool);
	void load_clicked(bool);
protected:
	QTimer *timer;
	QPushButton *startButton;
	QPushButton *stopButton;
	QPushButton *loadButton;

	QVBoxLayout *vLay;
	QHBoxLayout *hLay;

	Drawer *drawer;
};