#pragma once

#include <memory>
#include <string>
#include <vector>

class Type {
public:
	virtual void Eval(std::shared_ptr<Type> t) = 0;
	virtual void Apply(std::shared_ptr<Type> self) = 0;
	virtual bool ToBool() = 0;
	virtual std::string ToString() = 0;
	virtual std::shared_ptr<Type> Copy() = 0;
	virtual void Run(std::shared_ptr<Type> self) = 0;
};