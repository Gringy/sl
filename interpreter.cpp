#include "interpreter.h"
#include "types.h"

#include "utils.h"
#include "words.h"

#include <iostream>
#include <memory>

Interpreter *inter = nullptr;

Interpreter::Interpreter(const char *filename) {
	auto rootobj = std::make_shared<RootObj>(filename);
	runstack.push_back(rootobj);
	root = rootobj;
	inter = this;
	RegWords();
}

bool Interpreter::IsRunning() {
	auto root_ = std::dynamic_pointer_cast<RootObj>(root);
	return root_->running && !error;
}

void Interpreter::Tick() {
	if (!IsRunning()) return;
	if (runstack.size() == 0)
		throw InterError("runstack underflow: Interpreter::Tick");
	try {
		runstack.back()->Run(runstack.back());
	} catch (InterError e) {
		std::cerr << "ERROR: " << e.what() << std::endl;
		error = true;
	} catch (std::exception e) {
		std::cerr << "CORE_ERROR: " << e.what() << std::endl;
		error = true;
	}
	if (debugMode)
		Debug();
}

void Interpreter::DefineWord(const char *name, func interpretAction, func compileAction) {
	Word word;
	word.name = name;
	word.interpretAction = Func::newFunc(interpretAction);
	word.compileAction = Func::newFunc(compileAction);
	words.push_back(word);
}

void Interpreter::DefineWord(std::string name, 
				std::shared_ptr<Type> interpretAction, 
				std::shared_ptr<Type> compileAction) {
	Word word;
	word.name = name;
	word.interpretAction = interpretAction;
	word.compileAction = compileAction;
	words.push_back(word);
}

void Interpreter::Debug() {
	std::cerr << "stack:" << stack.size() << " compstack:"
		 << compstack.size() << " mode:" << (mode?"comp":"inter") 
		 << " runstack:" << runstack.size() << runstack.back()->ToString()
		 << '\n';
}

int Interpreter::PopInt() {
	if (stack.size() == 0) {
		throw InterError("stack underflow: Interpreter::PopInt");
	}
	Int *i1 = dynamic_cast<Int *>(stack.back().get());
	if (!i1) {
		throw InterError("type error: int expected: Interpreter::PopInt");
	}
	int ret = i1->value;
	stack.pop_back();
	return ret;
}
void Interpreter::PushInt(int n) {
	stack.push_back(Int::newInt(n));
}

bool Interpreter::PopBool() {
	if (stack.size() == 0) {
		throw InterError("stack underflow: Interpreter::PopBool");
	}
	Bool *i1 = dynamic_cast<Bool *>(stack.back().get());
	if (!i1) {
		throw InterError("type error: bool expected");
	}
	bool ret = i1->value;
	stack.pop_back();
	return ret;
}
void Interpreter::PushBool(bool n) {
	stack.push_back(Bool::newBool(n));
}

std::shared_ptr<Type> Interpreter::PopType() {
	if (stack.size() == 0) {
		throw InterError("stack underflow: Interpreter::PopType");
	}
	auto ret = stack.back();
	stack.pop_back();
	return ret;
}
void Interpreter::PushType(std::shared_ptr<Type> t) {
	stack.push_back(t);
}

void Interpreter::Compile(std::shared_ptr<Type> t) {
	if (compstack.size() == 0) {
		std::cerr << "cannot compile\n";
		return;
	}
	Quote *q = dynamic_cast<Quote*>(compstack[compstack.size()-1].get());
	if (!q)
		throw InterError("no quote in compile stack\n");
	q->value.push_back(t);
}

Word *Interpreter::SearchWord(std::string &name) {
	for (size_t i = 0; i < words.size(); i++)
		if (words[i].name == name)
			return &words[i];
	return nullptr;
}

void Interpreter::DropRunned() {
	if (runstack.size() == 0)
		throw InterError("runstack underflow");
	runstack.pop_back();
}

void Interpreter::AddToRun(std::shared_ptr<Type> t) {
	runstack.push_back(t);
}

std::istream &Interpreter::GetStream() {
	auto root_ = std::dynamic_pointer_cast<RootObj>(root);
	return *root_->in;
}
