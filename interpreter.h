#pragma once

#include <vector>
#include "basic_type.h"
#include "utils.h"

extern std::shared_ptr<Type> CompileFunc;

class Interpreter;
extern Interpreter *inter;

class Word {
public:
	std::string name;
	std::shared_ptr<Type> interpretAction;
	std::shared_ptr<Type> compileAction;
};

class Interpreter {
public:
	Interpreter(const char *filename);
	std::vector<std::shared_ptr<Type>> stack;
	std::vector<std::shared_ptr<Type>> compstack;
	std::vector<std::shared_ptr<Type>> runstack;
	std::vector<Word> words;
	bool mode;
	bool error;
	bool debugMode;

	void Tick();
	bool IsInInterpretMode() {
		return !mode;
	}
	bool IsInCompileMode() {
		return mode;
	}
	void DefineWord(const char *name, func interpretAction, func compileAction);
	void DefineWord(std::string name, 
				std::shared_ptr<Type> interpretAction, 
				std::shared_ptr<Type> compileAction);
	void Debug();

	int PopInt();
	void PushInt(int n);

	bool PopBool();
	void PushBool(bool n);

	std::shared_ptr<Type> root;

	std::shared_ptr<Type> PopType();
	void PushType(std::shared_ptr<Type> t);

	void Compile(std::shared_ptr<Type> t);

	Word *SearchWord(std::string &name);

	void DropRunned();
	void AddToRun(std::shared_ptr<Type> t);
	
	bool IsRunning();
	std::istream &GetStream();
};
