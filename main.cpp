#include <iostream>

#include "utils.h"
#include "interpreter.h"
#include "words.h"

#include <QApplication>
#include "MainWindow.h"

int main(int argc, char **argv) {
	new Interpreter("default.sl");
	QApplication app(argc, argv);
	MainWindow *window = new MainWindow();
	window->show();
	return app.exec();
}
