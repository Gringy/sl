######################################################################
# Automatically generated by qmake (2.01a) ?? ??? 3 13:06:31 2016
######################################################################

TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += .

# Input
QMAKE_CXXFLAGS += -std=c++14
HEADERS += basic_type.h \
           Drawer.h \
           interpreter.h \
           MainWindow.h \
           types.h \
           utils.h \
           words.h
SOURCES += Drawer.cpp \
           interpreter.cpp \
           main.cpp \
           MainWindow.cpp \
           types.cpp \
           utils.cpp \
           words.cpp
