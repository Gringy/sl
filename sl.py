import sys

lastChar = None
def getWordName():
	global lastChar
	ret = ""
	if lastChar:
		ret = lastChar
		lastChar = None
		return ret
	while True:
		ch = sys.stdin.read(1)
		if ch.isspace() and ret:
			return ret
		if ch.isspace() and not ret:
			continue
		if ch == "[" or ch == "]" or ch == '"':
			if not ret:
				return ch
			else:
				lastChar = ch
				return ret
		ret += ch

words = []
compstack = []
stack = []
comp = False

class Word:
	def __init__(self, name, comp, run):
		self.name = name
		self.comp = comp
		self.run = run
		words.append(self)

def search(name):
	for word in words:
		if word.name == name:
			return word
	return None

def toPush(obj):
	def func(): stack.append(obj)
	return func

def debug():
	print("stack:", stack)
	print("compstack:", compstack)
	print("comp:", comp)
	print("words:", len(words))

def apply(quote):
	for n in quote:
		n()

###############################################################################
def _if():
	quote = stack.pop()
	b = stack.pop()
	if b: apply(quote)
def compIf():
	compstack[-1].append(_if)
Word("if", compIf, _if)

def plus():
	stack.append(stack.pop() + stack.pop())
def compPlus():
	compstack[-1].append(plus)
Word("+", compPlus, plus)

def _print():
	print(stack.pop())
def compPrint():
	compstack[-1].append(_print)
Word("print", compPrint, _print)

def _exit():
	exit(0)
def compExit():
	compstack[-1].append(_exit)
Word("exit", compExit, _exit)


def _apply():
	quote = stack.pop()
	apply(quote)
def compApply():
	compstack[-1].append(_apply)
Word("apply", compApply, _apply)

def lbrk():
	global comp
	comp = True
	compstack.append([])
def compLbrk():
	compstack.append([])
Word("[", compLbrk, lbrk)

def rbrk():
	raise Exception("right bracket out of compile mode")
def compRbrk():
	global comp
	quote = compstack.pop()
	if not compstack:
		comp = False
		stack.append(quote)
	else:
		compstack[-1].append(toPush(quote))
Word("]", compRbrk, rbrk)

def getString():
	ch = ""
	ret = ""
	while ch != '"':
		ret += ch
		ch = sys.stdin.read(1)
	return ret
def string():
	s = getString()
	stack.append(s)
def compString():
	s = getString()
	compstack[-1].append(toPush(s))
Word('"', compString, string)

###############################################################################

while True:
	wordName = getWordName()
	word = search(wordName)
	if not word:
		if comp:
			compstack[-1].append(toPush(int(wordName)))
		else:
			stack.append(int(wordName))
	else:
		if comp:
			word.comp()
		else:
			word.run()
