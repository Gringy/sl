#include "types.h"
#include "interpreter.h"
#include "utils.h"
#include "words.h"

//#include <iostream>
#include <fstream>

///////////////////////////////////////////////////////////////////////////////
////////////////////          Int               ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////
void Int::Eval(std::shared_ptr<Type> t) {
	//inter->PushType(t);
	inter->AddToRun(t);
}
void Int::Apply(std::shared_ptr<Type>) {
	throw InterError("cannot apply int");
}
bool Int::ToBool() {
	return value;
}
std::string Int::ToString() {
	return std::to_string(value);
}
std::shared_ptr<Type> Int::Copy() {
	return std::shared_ptr<Type>(new Int(value));
}
std::shared_ptr<Type> Int::newInt(int n) {
	return std::shared_ptr<Type>(new Int(n));
}
void Int::Run(std::shared_ptr<Type> self) {
	inter->PushType(self);
	inter->DropRunned();
}
Int::~Int() {}

///////////////////////////////////////////////////////////////////////////////
////////////////////          Bool              ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////
void Bool::Eval(std::shared_ptr<Type> t) {
	//inter->PushType(t);
	inter->AddToRun(t);
}
void Bool::Apply(std::shared_ptr<Type>) {
	throw InterError("cannot apply bool");
}
bool Bool::ToBool() {
	return value;
}
std::string Bool::ToString() {
	if (value) return std::string("true");
	else return std::string("false");
}
std::shared_ptr<Type> Bool::Copy() {
	return std::shared_ptr<Type>(new Bool(value));
}
std::shared_ptr<Type> Bool::newBool(bool n) {
	return std::shared_ptr<Type>(new Bool(n));
}
void Bool::Run(std::shared_ptr<Type> self) {
	inter->PushType(self);
	inter->DropRunned();
}
Bool::~Bool() {}

///////////////////////////////////////////////////////////////////////////////
////////////////////          String            ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////
void String::Eval(std::shared_ptr<Type> t) {
	//inter->PushType(t);
	inter->AddToRun(t);
}
void String::Apply(std::shared_ptr<Type>) {
	throw InterError("cannot apply string");
}
bool String::ToBool() {
	return value.size() != 0;
}
std::string String::ToString() {
	std::string ret = "\"";
	ret = ret + value + "\"";
	return ret;
}
std::shared_ptr<Type> String::Copy() {
	return std::shared_ptr<Type>(new String(value));
}
std::shared_ptr<Type> String::newStr(std::string str) {
	return std::shared_ptr<Type>(new String(str));
}
void String::Run(std::shared_ptr<Type> self) {
	inter->PushType(self);
	inter->DropRunned();
}
String::~String() {}

///////////////////////////////////////////////////////////////////////////////
////////////////////          Func              ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////
void Func::Eval(std::shared_ptr<Type> t) {
	//value();
	inter->AddToRun(t);
}
void Func::Apply(std::shared_ptr<Type> self) {
	//value();
	inter->AddToRun(self);
}
bool Func::ToBool() {
	throw InterError("cannot cast func to bool");
}
std::string Func::ToString() {
	return std::string("<func>");
}
std::shared_ptr<Type> Func::Copy() {
	return std::shared_ptr<Type>(new Func(value));
}
std::shared_ptr<Type> Func::newFunc(func f) {
	return std::shared_ptr<Type>(new Func(f));
}
void Func::Run(std::shared_ptr<Type>) {
	inter->DropRunned();
	value();
}
Func::~Func() {}

///////////////////////////////////////////////////////////////////////////////
////////////////////          Quote             ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////
void Quote::Eval(std::shared_ptr<Type> t) {
	inter->PushType(t);
}
void Quote::Apply(std::shared_ptr<Type> self) {
	auto _self = std::dynamic_pointer_cast<Quote>(self);
	inter->AddToRun(std::make_shared<QuoteRunner>(_self));
}
bool Quote::ToBool() {
	return value.size() != 0;
}
std::string Quote::ToString() {
	std::string ret = "[";
	for (size_t i = 0; i < value.size(); i++)
		ret = ret + value[i]->ToString() + ' ';
	if (ret.length() == 1)
		ret += ']';
	else
		ret[ret.length()-1] = ']';
	return ret;
}
std::shared_ptr<Type> Quote::Copy() {
	Quote *q = new Quote();
	q->value = value;
	return std::shared_ptr<Type>(q);
}
void Quote::Run(std::shared_ptr<Type> self) {
	inter->DropRunned();
	inter->PushType(self);
}
Quote::~Quote() {}

///////////////////////////////////////////////////////////////////////////////
////////////////////          RootObj           ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////
RootObj::RootObj(const char *filename) {
	in = new std::ifstream(filename);
	running = true;
}
void RootObj::Eval(std::shared_ptr<Type>) {
	throw InterError("");
}
void RootObj::Apply(std::shared_ptr<Type>) {
	throw InterError("");
}
bool RootObj::ToBool() {
	throw InterError("");
}
std::string RootObj::ToString() {
	return std::string("<RootObj>");
}
std::shared_ptr<Type> RootObj::Copy() {
	throw InterError("");
}
void RootObj::Run(std::shared_ptr<Type>) {
	std::istream &input = *in;
	std::string wordName = getWordName(input);
	if (wordName.length() == 0) {
		running = false;
		return;
	}
	Word *word = inter->SearchWord(wordName);
	if (word) {
		if (!inter->mode)
			word->interpretAction->Apply(word->interpretAction);
			//inter->AddToRun(word->interpretAction);
		else
			word->compileAction->Apply(word->compileAction);
			//inter->AddToRun(word->compileAction);
	} else {
		int num = stringToInt(wordName);
		//auto numInt = Int::newInt(num);
		if (!inter->mode)
			inter->PushInt(num);
		else
			inter->Compile(Int::newInt(num));
	}
}
RootObj::~RootObj() {
	delete in;
}

///////////////////////////////////////////////////////////////////////////////
////////////////////          QuoteRunner       ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////
QuoteRunner::QuoteRunner(std::shared_ptr<Quote> q) {
	quote = q;
	n = 0;
}
void QuoteRunner::Eval(std::shared_ptr<Type>) {
	throw InterError("");
}
void QuoteRunner::Apply(std::shared_ptr<Type>) {
	throw InterError("");
}
bool QuoteRunner::ToBool() {
	throw InterError("");
}
std::string QuoteRunner::ToString() {
	return std::string("<QuoteRunner>");
}
std::shared_ptr<Type> QuoteRunner::Copy() {
	throw InterError("");
}
void QuoteRunner::Run(std::shared_ptr<Type>) {
	if (n < quote->value.size()) {
		inter->runstack.push_back(quote->value[n]);
		//quote->value[n]->Apply(quote->value[n]);
		n++;
	} else {
		inter->runstack.pop_back();
	}
}
QuoteRunner::~QuoteRunner() {}

///////////////////////////////////////////////////////////////////////////////
////////////////////          LoopRunner        ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////
LoopRunner::LoopRunner(std::shared_ptr<Type> test, std::shared_ptr<Type> quote) {
	i = 0;
	mode = testing;
	this->test = test;
	this->quote = quote;
}
void LoopRunner::Eval(std::shared_ptr<Type>) {
	throw InterError("");
}
void LoopRunner::Apply(std::shared_ptr<Type>) {
	throw InterError("");
}
bool LoopRunner::ToBool() {
	throw InterError("");
}
std::string LoopRunner::ToString() {
	return std::string("<LoopRunner>");
}
std::shared_ptr<Type> LoopRunner::Copy() {
	throw InterError("");
}
void LoopRunner::Run(std::shared_ptr<Type>) {
	if (mode == testing) {
		test->Apply(test);
		mode = running;
	} else if (mode == running) {
		if (inter->PopType()->ToBool())
			quote->Apply(quote);
		else
			inter->DropRunned();
		mode = adding;
	} else {
		i++;
		mode = testing;
	}
}
LoopRunner::~LoopRunner() {}
