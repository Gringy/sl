#pragma once

#include <memory>
#include <string>
#include <iostream>

#include "basic_type.h"

class Int : public Type {
public:
	Int() { value = 0; }
	Int(int _value) { value = _value; }
	int value;

	void Eval(std::shared_ptr<Type> t);
	void Apply(std::shared_ptr<Type> self);
	bool ToBool();
	std::string ToString();
	std::shared_ptr<Type> Copy();
	void Run(std::shared_ptr<Type> self);
	static std::shared_ptr<Type> newInt(int n);
	~Int();
};


class Bool : public Type {
public:
	Bool() { value = 0; }
	Bool(bool _value) { value = _value; }
	bool value;

	void Eval(std::shared_ptr<Type> t);
	void Apply(std::shared_ptr<Type> self);
	bool ToBool();
	std::string ToString();
	std::shared_ptr<Type> Copy();
	void Run(std::shared_ptr<Type> self);
	static std::shared_ptr<Type> newBool(bool n);
	~Bool();
};


class String : public Type {
public:
	String(){}
	String(std::string str) { value=str; }
	String(const char *str) { value=str; }
	std::string value;

	void Eval(std::shared_ptr<Type> t);
	void Apply(std::shared_ptr<Type> self);
	bool ToBool();
	std::string ToString();
	std::shared_ptr<Type> Copy();
	void Run(std::shared_ptr<Type> self);
	static std::shared_ptr<Type> newStr(std::string str);
	~String();
};

class Func : public Type {
public:
	typedef void(*func)(void);
	Func() { value = nullptr; }
	Func(func _value) { value = _value; }
	func value;

	void Eval(std::shared_ptr<Type> t);
	void Apply(std::shared_ptr<Type> self);
	bool ToBool();
	std::string ToString();
	std::shared_ptr<Type> Copy();
	void Run(std::shared_ptr<Type> self);
	static std::shared_ptr<Type> newFunc(func f);
	~Func();
};

class Quote : public Type {
public:
	std::vector<std::shared_ptr<Type>> value;

	void Eval(std::shared_ptr<Type> t);
	void Apply(std::shared_ptr<Type> self);
	bool ToBool();
	std::string ToString();
	std::shared_ptr<Type> Copy();
	void Run(std::shared_ptr<Type> self);
	~Quote();
};

class RootObj : public Type {
public:
	RootObj(const char *filename);
	void Eval(std::shared_ptr<Type> t);
	void Apply(std::shared_ptr<Type> self);
	bool ToBool();
	std::string ToString();
	std::shared_ptr<Type> Copy();
	void Run(std::shared_ptr<Type> self);
	bool running;
	std::istream *in;
	~RootObj();
};

class QuoteRunner : public Type {
public:
	QuoteRunner(std::shared_ptr<Quote> q);
	void Eval(std::shared_ptr<Type> t);
	void Apply(std::shared_ptr<Type> self);
	bool ToBool();
	std::string ToString();
	std::shared_ptr<Type> Copy();
	void Run(std::shared_ptr<Type> self);

	std::shared_ptr<Quote> quote;
	size_t n;
	~QuoteRunner();
};

class LoopRunner : public Type {
public:
	LoopRunner(std::shared_ptr<Type> test, std::shared_ptr<Type> quote);
	void Eval(std::shared_ptr<Type> t);
	void Apply(std::shared_ptr<Type> self);
	bool ToBool();
	std::string ToString();
	std::shared_ptr<Type> Copy();
	void Run(std::shared_ptr<Type> self);

	int i;
	std::shared_ptr<Type> test;
	std::shared_ptr<Type> quote;
	int mode;
	enum {
		testing = 0,
		running = 1,
		adding  = 2
	};
	~LoopRunner();
};