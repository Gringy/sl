#include "utils.h"

std::string getWordName(std::istream &in) {
	static char lastChar;
	std::string ret;
	int ch;
	if (lastChar) {
		ret.push_back(lastChar);
		lastChar = 0;
		return ret;
	}
	while (true) {
		ch = in.get();
		if (ch == -1)
			return ret;
		if (isspace(ch) && ret.length() != 0)
			return ret;
		if (isspace(ch) && ret.length() == 0)
			continue;
		if (ch == '[' || ch == ']' || ch == '"') {
			if (ret.length() == 0) {
				char str[2] = {0, 0};
				str[0] = ch;
				return std::string(str);
			} else {
				lastChar = ch;
				return ret;
			}
		}
		ret += ch;
	}
	return ret;
}

int stringToInt(std::string str) {
	int ret = 0;
	for (size_t i = 0; i < str.length(); i++) {
		if (!isdigit(str[i]))
			throw InterError("unknown digit in number");
		ret = ret*10 + str[i] - '0';
	}
	return ret;
}
