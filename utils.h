#pragma once

#include <string>
#include <iostream>
#include <exception>


class InterError : public std::exception {
public:
	InterError(const char *err) { mess = err; }
	const char *mess;
	const char *which() { return mess; }
	const char *what() { return mess; }
};

typedef void(*func)(void);

std::string getWordName(std::istream &in);
int stringToInt(std::string str);
