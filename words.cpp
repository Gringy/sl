#include "words.h"
#include "types.h"
#include "interpreter.h"
#include "utils.h"

#include "Drawer.h"

#include <iostream>
#include <cstdlib>

void ElInter() {
	int rad = inter->PopInt();
	int y = inter->PopInt();
	int x = inter->PopInt();
	defaultDrawer->drawEllipse(x, y, rad);
}
void ElComp() {
	inter->Compile(Func::newFunc(ElInter));
}

void PointInter() {
	int y = inter->PopInt();
	int x = inter->PopInt();
	defaultDrawer->drawPoint(x, y);
}
void PointComp() {
	inter->Compile(Func::newFunc(PointInter));
}

void DrawInter() {
	defaultDrawer->repaint();
}
void DrawComp() {
	inter->Compile(Func::newFunc(DrawInter));
}

void ColorInter() {
	int b = inter->PopInt();
	int g = inter->PopInt();
	int r = inter->PopInt();
	defaultDrawer->setColor(r, g, b);
}
void ColorComp() {
	inter->Compile(Func::newFunc(ColorInter));
}

void ClearInter() {
	defaultDrawer->clear();
}
void ClearComp() {
	inter->Compile(Func::newFunc(ClearInter));
}

void AddInter() {
	auto i = inter->PopInt() + inter->PopInt();
	inter->PushInt(i);
}
void AddComp() {
	inter->Compile(Func::newFunc(AddInter));
}

void SubInter() {
	int i2 = inter->PopInt();
	int i1 = inter->PopInt();
	int i = i1 - i2;
	inter->PushInt(i);
}
void SubComp() {
	inter->Compile(Func::newFunc(SubInter));
}

void MulInter() {
	int i2 = inter->PopInt();
	int i1 = inter->PopInt();
	int i = i1 * i2;
	inter->PushInt(i);
}
void MulComp() {
	inter->Compile(Func::newFunc(MulInter));
}

void DivInter() {
	int i2 = inter->PopInt();
	int i1 = inter->PopInt();
	int i = i1 / i2;
	inter->PushInt(i);
}
void DivComp() {
	inter->Compile(Func::newFunc(DivInter));
}

void ModInter() {
	int i2 = inter->PopInt();
	int i1 = inter->PopInt();
	auto i = i1 % i2;
	inter->PushInt(i);
}
void ModComp() {
	inter->Compile(Func::newFunc(ModInter));
}

void RandInter() {
	inter->PushInt(rand());
}
void RandComp() {
	inter->Compile(Func::newFunc(RandInter));
}
///////////////////////////////////////////////////////////////////////////////
void PrintInter() {
	auto i1 = inter->PopType(); 
	std::cerr << i1->ToString() << " ";
}
void PrintComp() {
	inter->Compile(Func::newFunc(PrintInter));
}

void ExitInter() {
	inter->error = true;
}
void ExitComp() {
	inter->Compile(Func::newFunc(ExitInter));
}

void LbrkInterComp() {
	inter->mode = true;
	inter->compstack.push_back(std::shared_ptr<Quote>(new Quote));
}

void RbrkComp() {
	if (inter->compstack.size() == 0)
		throw InterError("compstack underflow");
	std::shared_ptr<Quote> q = std::dynamic_pointer_cast<Quote>(inter->compstack.back());
	inter->compstack.pop_back();
	if (inter->compstack.size() == 0) inter->mode = false;
	if (inter->mode)
		inter->Compile(q);
	else inter->PushType(q);
}
void RbrkInter() {
	throw InterError("right bracket without compile mode");
}

std::string readString() {
	std::string ret;
	char ch = 0;
	while (true) {
		ch = inter->GetStream().get();
		if (ch == '"') break;
		ret += ch;
	}
	return ret;
}
void StringInter() {
	auto str = readString();
	inter->PushType(String::newStr(str));
}
void StringComp() {
	auto str = readString();
	inter->Compile(String::newStr(str));
}

void IfInter() {
	auto quote = inter->PopType();
	auto b = inter->PopType();
	if (b->ToBool())
		quote->Apply(quote);
}
void IfComp() {
	inter->Compile(Func::newFunc(IfInter));
}

void ApplyInter() {
	auto q = inter->PopType();
	q->Apply(q);
}
void ApplyComp() {
	inter->Compile(Func::newFunc(ApplyInter));
}
auto ApplyFunc = Func::newFunc(ApplyInter);

void CompileInter() {
	inter->Compile(inter->PopType());
	inter->Compile(ApplyFunc);
}
void CompileComp() {
	inter->Compile(Func::newFunc(CompileInter));
}
std::shared_ptr<Type> CompileFunc = Func::newFunc(CompileInter);

void WordInter() {
	auto nameType = inter->PopType();
	auto quote = inter->PopType();
	auto nameString = std::dynamic_pointer_cast<String>(nameType);
	if (!nameString)
		throw InterError("word name must be string");
	std::string &name = nameString->value;
	Quote *q = new Quote();
	q->value.push_back(quote);
	q->value.push_back(CompileFunc);
	Word *found = inter->SearchWord(name);
	if (!found) {
		inter->DefineWord(name, quote, std::shared_ptr<Type>(q));
	} else {
		found->interpretAction = quote;
		found->compileAction = std::shared_ptr<Type>(q);
	}
}
void WordComp() {
	inter->Compile(Func::newFunc(WordInter));
}

void ToStringInter() {
	auto t = inter->PopType();
	inter->PushType(String::newStr(t->ToString()));
}
void ToStringComp() {
	inter->Compile(Func::newFunc(ToStringInter));
}

void LoopInter() {
	auto quote = inter->PopType();
	auto test = inter->PopType();
	inter->AddToRun(std::make_shared<LoopRunner>(test, quote));
}
void LoopComp() {
	inter->Compile(Func::newFunc(LoopInter));
}
///////////////////////////////////////////////////////////////////////////////
void DupInter() {
	auto t = inter->PopType();
	inter->PushType(t);
	inter->PushType(t->Copy());
}
void DupComp() {
	inter->Compile(Func::newFunc(DupInter));
}

void DropInter() {
	inter->PopType();
}
void DropComp() {
	inter->Compile(Func::newFunc(DropInter));
}

void SwapInter() {
	auto t1 = inter->PopType();
	auto t2 = inter->PopType();
	inter->PushType(t1);
	inter->PushType(t2);
}
void SwapComp() {
	inter->Compile(Func::newFunc(SwapInter));
}

void RotInter() {
	auto t1 = inter->PopType();
	auto t2 = inter->PopType();
	auto t3 = inter->PopType();
	inter->PushType(t2);
	inter->PushType(t1);
	inter->PushType(t3);
}
void RotComp() {
	inter->Compile(Func::newFunc(RotInter));
}
///////////////////////////////////////////////////////////////////////////////
void EqInter() {
	inter->PushBool(inter->PopInt() == inter->PopInt());
}
void EqComp() {
	inter->Compile(Func::newFunc(EqInter));
}

void NeqInter() {
	inter->PushBool(inter->PopInt() != inter->PopInt());
}
void NeqComp() {
	inter->Compile(Func::newFunc(NeqInter));
}

void BiggerInter() {
	int i2 = inter->PopInt();
	int i1 = inter->PopInt();
	auto i = Bool::newBool(i1 > i2);
	inter->PushType(i);
}
void BiggerComp() {
	inter->Compile(Func::newFunc(BiggerInter));
}

void LessInter() {
	int i2 = inter->PopInt();
	int i1 = inter->PopInt();
	auto i = Bool::newBool(i1 < i2);
	inter->PushType(i);
}
void LessComp() {
	inter->Compile(Func::newFunc(LessInter));
}

void BiggerEqInter() {
	int i2 = inter->PopInt();
	int i1 = inter->PopInt();
	auto i = Bool::newBool(i1 >= i2);
	inter->PushType(i);
}
void BiggerEqComp() {
	inter->Compile(Func::newFunc(BiggerEqInter));
}

void LessEqInter() {
	int i2 = inter->PopInt();
	int i1 = inter->PopInt();
	auto i = Bool::newBool(i1 <= i2);
	inter->PushType(i);
}
void LessEqComp() {
	inter->Compile(Func::newFunc(BiggerEqInter));
}

void IInter() {
	for (int i = inter->runstack.size()-1; i != -1; i--) {
		auto type = inter->runstack[i];
		auto runner = std::dynamic_pointer_cast<LoopRunner>(type);
		if (runner) {
			inter->PushInt(runner->i);
			return;
		}
	}
	throw InterError("no loops");
}
void IComp() {
	inter->Compile(Func::newFunc(IInter));
}

void JInter() {
	bool b = false;
	for (int i = inter->runstack.size()-1; i != -1; i--) {
		auto type = inter->runstack[i];
		auto runner = std::dynamic_pointer_cast<LoopRunner>(type);
		if (runner) if (b) {
			inter->PushInt(runner->i);
			return;
		} else b = true;
	}
	throw InterError("no loops");
}
void JComp() {
	inter->Compile(Func::newFunc(JInter));
}

void TrueInter() {
	inter->PushBool(true);
}
void TrueComp() {
	inter->Compile(Func::newFunc(TrueInter));
}

void FalseInter() {
	inter->PushBool(false);
}
void FalseComp() {
	inter->Compile(Func::newFunc(FalseInter));
}

void DebugInter() {
	inter->debugMode = inter->PopBool();
}
void DebugComp() {
	inter->Compile(Func::newFunc(FalseInter));
}

void RegWords() {
	// math words
	inter->DefineWord("+",     AddInter,      AddComp);
	inter->DefineWord("-",     SubInter,      SubComp);
	inter->DefineWord("*",     MulInter,      MulComp);
	inter->DefineWord("/",     DivInter,      DivComp);
	inter->DefineWord("%",     ModInter,      ModComp);
	inter->DefineWord("rand",  RandInter,     RandComp);

	// qt draw words
	inter->DefineWord("ellipse",ElInter,       ElComp);
	inter->DefineWord("point",  PointInter,    PointComp);
	inter->DefineWord("draw",   DrawInter,     DrawComp);
	inter->DefineWord("color",  ColorInter,    ColorComp);
	inter->DefineWord("clear",  ClearInter,    ClearComp);

	// compiler words
	inter->DefineWord("print",  PrintInter,    PrintComp);
	inter->DefineWord("exit",   ExitInter,     ExitComp);
	inter->DefineWord("[",      LbrkInterComp, LbrkInterComp);
	inter->DefineWord("]",      RbrkInter,     RbrkComp);
	inter->DefineWord("apply",  ApplyInter,    ApplyComp);
	inter->DefineWord("compile",CompileInter,  CompileComp);
	inter->DefineWord("if",     IfInter,       IfComp);
	inter->DefineWord("\"",     StringInter,   StringComp);
	inter->DefineWord("(str)",  ToStringInter, ToStringComp);
	inter->DefineWord("word",   WordInter,     WordComp);

	//loop words
	inter->DefineWord("for",    LoopInter,     LoopComp);
	inter->DefineWord("i",      IInter,        IComp);
	inter->DefineWord("j",      JInter,        JComp);

	//stack word words
	inter->DefineWord("dup",    DupInter,      DupComp);
	inter->DefineWord("drop",   DropInter,     DropComp);
	inter->DefineWord("swap",   SwapInter,     SwapComp);
	inter->DefineWord("rot",    RotInter,      RotComp);

	// integer compatarion words
	inter->DefineWord("==",     EqInter,       EqComp);
	inter->DefineWord("!=",     NeqInter,      NeqComp);
	inter->DefineWord(">",      BiggerInter,   BiggerComp);
	inter->DefineWord("<",      LessInter,     LessComp);
	inter->DefineWord(">=",     BiggerEqInter, BiggerEqComp);
	inter->DefineWord("<=",     LessEqInter,   LessEqComp);

	inter->DefineWord("true",   TrueInter,     TrueComp);
	inter->DefineWord("false",  FalseInter,    FalseComp);
}